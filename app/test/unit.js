const {
  APIError,
  parsePageLimit
} = require('../helpers');
const {
  Order
} = require('../models');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect;

describe('Parse page limit helper function', () => {
  describe('parsePageLimit()', () => {
    it('should return a number if a valid limit is passed', () => {
      const limit = '6';
      const validatedLimit = parsePageLimit(limit);
      expect(validatedLimit).equal(6);
    });
    it('should return an API Error if a non-numeric limit is passed', () => {
      const limit = 'foo';
      const validatedLimit = parsePageLimit(limit);
      expect(validatedLimit).to.be.an.instanceof(APIError);
    });
    it('should return an API Error if zero is passed when type is "limit"', () => {
      const limit = '0';
      const validatedLimit = parsePageLimit(limit);
      expect(validatedLimit).to.be.an.instanceof(APIError);
    });
    it('should return an API Error if a negative limit is passed', () => {
      const limit = '-1';
      const validatedLimit = parsePageLimit(limit);
      expect(validatedLimit).to.be.an.instanceof(APIError);
    });
    it('should return a number if a valid numeric limit is passed', () => {
      const limit = '5';
      const validatedLimit = parsePageLimit(limit);
      expect(validatedLimit).equal(5);
    });
  });
});

describe('APIError module', () => {
  describe('APIError', () => {
    it('error reponse contain status and message property', () => {
      const response = new APIError(409, `ORDER_ALREADY_BEEN_TAKEN`);
      expect(response).to.have.property('status');
      expect(response).to.have.property('message');
    });
  });
});

// describe('Order Model', () => {
//   describe('order create', () => {
//       it('should create order', () => {
//           const orderData = {
//               id: Math.random(),
//               distance: 100,
//               status: "UNASSIGNED",
//               origin: [
//               "28.7041",
//               "77.1025"
//             ],
//             destination: [
//               "28.4595",
//               "77.0266"
//             ],
//         }
//         const newOrder = await Order.createOrder(new Order(orderData));
//         newOrder.then(function(result) {
//            console.log(result);
//         })
//       });
//   });
// });
